<?php include_once('header.php'); ?>
<div class="mainContainerQuiz">
	<?php if (isset($_GET['category_id'])) {
		$category_id = $_GET['category_id'];
	?>
	<div class="container">
		<div class="well mainSection">
			<?php
				
				$categories = mysqli_query($con, "SELECT * FROM `category` WHERE `id`='$category_id'");
            while ($category = mysqli_fetch_assoc($categories)) {
                $id = $category['id'];
                $cat_title = $category['title'];
            }
			?>
			
			<h3 class="marginZero"><?php echo $cat_title; ?></h3>
		</div>
		<div class="col-md-9 mainSectionLeft">
			<?php  
            $quizzes = mysqli_query($con, "SELECT * FROM `quizzes` WHERE `category_id`='$category_id' AND `status`=1");
            while ($quiz = mysqli_fetch_assoc($quizzes)) {
                $quiz_id = $quiz['id'];
                $category_id = $quiz['category_id'];
                $quiz_title = $quiz['title'];
                $opening_date = $quiz['opening_date'];
                $closing_date = $quiz['closing_date'];
                $status = $quiz['status'];
                $created_at = $quiz['created_at'];
            ?>
			<div class="col-md-6 paddingZero">
				<div class="quizSection">
					<a href="quiz.php?quiz_id=<?php echo $quiz_id; ?>"><i class="fas fa-globe"></i> <?php echo $quiz_title; ?></a>
				</div>
			</div>
		<?php } ?>
		</div>
		<div class="col-md-3 mainSectionRight">
			<h3 class="marginZero">General Information</h3>
			<?php include_once('rightbarLinks.php'); ?>
			<div class="col-md-12 gads paddingZero">
				<img src="img/gad.png" class="img img-responsive">
			</div>
		</div>      
	</div>

<?php }else{ ?>
	<div class="container">
		<div class="well mainSection">
			<h3 class="marginZero">Thank you for visit us.</h3>
		</div>
		<div class="col-md-9 mainSectionLeft">
			<h3 class="marginZero">No data found !!!</h3>
		</div>
		<div class="col-md-3 mainSectionRight">
			<h3 class="marginZero">General Information</h3>
			<?php include_once('rightbarLinks.php'); ?>
			<div class="col-md-12 gads paddingZero">
				<img src="img/gad.png" class="img img-responsive">
			</div>
		</div> 
	</div>
<?php } ?>
</div>
<?php include_once('footer.php'); ?>