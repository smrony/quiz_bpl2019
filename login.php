<?php include_once('header.php');?>
<?php include_once('inlog.php');?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 login-box">
				<div class="login-logo text-center">
			      <img src="img/logo.png" class="">
			    </div>
			    <h3>Member Login</h3>
			    <p>Please enter your email and password</p>
			    <?php if(!empty($err)){ ?>
                  <h2 class="panel-title text-center" style="color:red; padding: 10px 0px;"><?php echo $err; ?></h2>
                <?php } ?>
				<form method="post" action="inlog.php">
					<div class="form-group">
						<label>Email</label>
				        <input type="email" name="email" class="form-control" placeholder="Email" required>
				    </div>
				    <div class="form-group">
				    	<label>Password</label>
				        <input type="password" name="password" class="form-control" placeholder="Password" required>
				    </div>
				    <div class="form-group">
				    	<button type="submit" name="in" class="btn btn-success btn-block btn-flat">Login</button>
				    </div>
				    <h3>OR</h3>
				    <div class="form-group">
				    	<a href="register.php" class="btn btn-warning btn-block btn-flat">Register as a new member</a>
				    </div>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</section>
<?php include_once('footer.php');?>