<?php include_once('header.php'); ?>
<?php include_once('submitAnswer.php'); ?>
<div class="mainContainerQuiz">
	<?php if (isset($_GET['quiz_id'])) {
		$quiz_id = $_GET['quiz_id'];
	?>
	<div class="container">
		<div class="well mainSection">
			<?php 
				// $quiz_id = $_GET['quiz_id'];
				$quizzes = mysqli_query($con, "SELECT * FROM `quizzes` WHERE `id`='$quiz_id'");
            while ($quiz = mysqli_fetch_assoc($quizzes)) {
                $quiz_id = $quiz['id'];
                $category = $quiz['category_id'];
                $quiz_title = $quiz['title'];
            }

			?>
			<h3 class="marginZero"><?php echo $quiz_title; ?></h3>
		</div>
		<div class="col-md-9 mainSectionLeft">
			<div class="questionSection">
				<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">   
                    <?php  
                        $questions = mysqli_query($con, "SELECT * FROM `questions` WHERE `quiz_id`='$quiz_id' AND `status`=1");
                        while ($question = mysqli_fetch_assoc($questions)) {
                            $question_id = $question['id'];
                            $category_id = $question['category_id'];
                            $quiz_id = $question['quiz_id'];
                            $ques = $question['question'];
                            $right_answer = $question['right_answer'];
                    ?>
					<div class="form-group">
						<label>
							<strong><?php echo $ques; ?></strong>
						</label>
						<pre>All Capital Letter</pre>
						<input type="text" name="answer" placeholder="Write Your Answer here (All Capital Letter)" class="form-control" required>

						<input type="hidden" name="category_id" value="<?php echo $category_id; ?>">
						<input type="hidden" name="quiz_id" value="<?php echo $quiz_id; ?>">
						<input type="hidden" name="member_id" value="<?php echo $userID; ?>">
						<input type="hidden" name="question_id" value="<?php echo $question_id; ?>">
						<input type="hidden" name="right_answer" value="<?php echo $right_answer; ?>">
					</div>
				<?php } ?> 
					
					<input type="submit" name="submitAnswer" value="Submit Answer" class="btn btn-success pull-right">
				</form>
			</div>
		</div>
		<div class="col-md-3 mainSectionRight">
			<h3 class="marginZero">General Information</h3>
			<?php include_once('rightbarLinks.php'); ?>
			<div class="col-md-12 gads paddingZero">
				<img src="img/gad.png" class="img img-responsive">
			</div>
		</div>      
	</div>
<?php }else{ ?>
	<div class="container">
		<div class="well mainSection">
			<?php if ($insert) { ?>
				<h3 class="marginZero">Thank you for your perticipation.</h3>
			<?php }elseif($err2){ ?> 
			<h3 class="marginZero"> You have to <a href="register.php">Register</a> first to perticipate  Quiz. Thank you for visit us.</h3>
		<?php } ?>
		</div>
		<div class="col-md-9 mainSectionLeft">
			<?php if($err){ ?> 
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong><?php echo $err; ?></strong> 
                </div>
            <?php } ?>
			<!---<h3 class="marginZero">No data found !!!</h3>-->
		</div>
		<div class="col-md-3 mainSectionRight">
			<h3 class="marginZero">General Information</h3>
			<?php include_once('rightbarLinks.php'); ?>
			<div class="col-md-12 gads paddingZero">
				<img src="img/gad.png" class="img img-responsive">
			</div>
		</div> 
	</div>
<?php } ?>
</div>
<?php include_once('footer.php'); ?>