<?php include_once('header.php'); ?>
<div class="mainContainerQuiz">
	<div class="container">
		<div class="well mainSection">
			<h3 class="marginZero">Our Proud Members</h3>
		</div>
		 <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>Member List </strong>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Name</th> 
                                        <th>Gender</th>
                                        <th>Joined</th>
                                        <th>Quiz Attended</th>
                                        <th>Total Point</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sl = 1;
                                    $members = mysqli_query($con, "SELECT * FROM `members`");
                                    while ($member = mysqli_fetch_assoc($members)) {
                                        $id = $member['id'];
                                        $name = $member['name'];
                                        $sex = $member['sex'];  
                                        $joined = $member['created_at'];  
                                        $win = $member['win'];  
                                        $point = $member['point'];  
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sl; ?></td>
                                        <td><?php echo $name; ?></td>
                                        <td><?php echo $sex; ?></td>
                                        <td>
                                            <?php $date=date_create("$joined");
                                                  echo date_format($date,"Y - M - d"); 
                                            ?>
                                        </td>
                                        <td>0<?php echo $win; ?></td>
                                        <td>0<?php echo $point; ?></td>
                                    </tr>
                                <?php $sl++; } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
		       
	</div>
</div>
<?php include_once('footer.php'); ?>