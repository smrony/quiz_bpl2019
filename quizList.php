<?php include_once('header.php'); ?>
<div class="mainContainerQuiz">
	<div class="container">
		<div class="well mainSection">
			<h3 class="marginZero">Main Section</h3>
		</div>
		<div class="col-md-9 mainSectionLeft">
			<?php 
            $categories = mysqli_query($con, "SELECT * FROM `category` WHERE `status`=1");
            while ($category = mysqli_fetch_assoc($categories)) {
                $id = $category['id'];
                $title = $category['title'];
                $status = $category['status'];
                $created_at = $category['created_at'];
            ?>
			<div class="col-md-6 paddingZero">
				<div class="quizSection">
					<a href="quizDetails.php?category_id=<?php echo $id; ?>"><i class="fas fa-globe"></i> <?php echo $title; ?></a>
				</div>
			</div>
		<?php } ?>
		</div>
		<div class="col-md-3 mainSectionRight">
			<h3 class="marginZero">General Information</h3>
			<?php include_once('rightbarLinks.php'); ?>
			<div class="col-md-12 gads paddingZero">
				<img src="img/gads.jpg" class="img img-responsive">
			</div>
		</div>      
	</div>
</div>
<?php include_once('footer.php'); ?>