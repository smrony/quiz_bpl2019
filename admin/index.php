<?php include_once('header.php'); ?>
<?php include_once('leftSideNav.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <?php 
                                        $members = mysqli_query($con, "SELECT * FROM `members`");
                                        if($members) {
                                          $total_member = mysqli_num_rows($members);
                                          ?>
                                          <div class="huge"><?php echo $total_member; ?></div>
                                        <?php } else { ?>
                                          <div class="huge">000</div>
                                        <?php } ?>
                                    <div>New Members</div>
                                </div>
                            </div>
                        </div>
                        <a href="members.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-globe fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <?php 
                                        $quizzes = mysqli_query($con, "SELECT * FROM `quizzes`");
                                        if($quizzes) {
                                          $total_quiz = mysqli_num_rows($quizzes);
                                          ?>
                                          <div class="huge"><?php echo $total_quiz; ?></div>
                                        <?php } else { ?>
                                          <div class="huge">000</div>
                                        <?php } ?>
                                    <div>Total Quiz</div>
                                </div>
                            </div>
                        </div>
                        <a href="manageQuiz.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-download fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <?php 
                                        $answers = mysqli_query($con, "SELECT * FROM `answers`");
                                        if($answers) {
                                          $total_answer = mysqli_num_rows($answers);
                                          ?>
                                          <div class="huge"><?php echo $total_answer; ?></div>
                                        <?php } else { ?>
                                          <div class="huge">000</div>
                                        <?php } ?>
                                    <div>Today's Submission</div>
                                </div>
                            </div>
                        </div>
                        <a href="result.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <?php 
                                        $members = mysqli_query($con, "SELECT * FROM `members`");
                                        if($members) {
                                          $total_member = mysqli_num_rows($members);
                                          ?>
                                          <div class="huge"><?php echo $total_member; ?></div>
                                        <?php } else { ?>
                                          <div class="huge">000</div>
                                        <?php } ?>
                                    <div>Total Member</div>
                                </div>
                            </div>
                        </div>
                        <a href="members.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php include_once('footer.php'); ?>