<?php include_once('header.php'); ?>
<?php include_once('leftSideNav.php'); ?>
<?php include_once('insertQuestion.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Question</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <?php if($insert){ ?> 
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> A New Question Added!!!
                        </div>
                    <?php } ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Enter Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Quiz Category</label>
                                            <select name="quizCategory" class="form-control">
                                                <option>Select a Category</option>
                                                <?php 
                                                    $categories = mysqli_query($con, "SELECT * FROM `category`");
                                                    while ($category = mysqli_fetch_assoc($categories)) {
                                                        $title = $category['title'];
                                                        $cat_id = $category['id'];
                                                ?>
                                                <option value="<?php echo $cat_id; ?>"><?php echo $title; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Quiz Title</label>
                                            <select name="quizTitle" class="form-control">
                                                <option>Select a Quiz Title</option>
                                                <?php 
                                                    $quizzes = mysqli_query($con, "SELECT * FROM `quizzes`");
                                                    while ($quiz = mysqli_fetch_assoc($quizzes)) {
                                                        $title = $quiz['title'];
                                                        $quiz_id = $quiz['id'];
                                                ?>
                                                <option value="<?php echo $quiz_id; ?>"><?php echo $title; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Question</label>
                                            <input type="text" class="form-control" name="question" placeholder="Who is the height scorer in 1st match ?">
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Answer</label>
                                            <input type="text" class="form-control" name="answer"  placeholder="Tamim Iqbal">
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Status</label>
                                            <select name="status" class="form-control">
                                                <option>Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Deactive</option>
                                            </select>
                                        </div> 
                                        <input type="submit" name="addQuestion" value="Add Question" class="btn btn-success btn-md">
                                    </form>
                                </div>
                            </div>
                        </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('footer.php'); ?>