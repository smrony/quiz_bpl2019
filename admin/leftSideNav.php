<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="members.php"><i class="fa fa-dashboard fa-fw"></i> Members</a>
                        </li>
                        <li>
                            <a href="manageCategory.php"><i class="fa fa-dashboard fa-fw"></i> All Category</a>
                        </li>
                        <li>
                            <a href="manageQuiz.php"><i class="fa fa-dashboard fa-fw"></i> All Quiz</a>
                        </li>
                        <li>
                            <a href="manageQuestion.php"><i class="fa fa-dashboard fa-fw"></i> All Question</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> RESULTS<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="manageQuiz.php">Quiz Wise Result</a>
                                </li>
                                <li>
                                    <a href="manageQuestion.php">Question Wise Result</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> QUIZ<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="addCategory.php">Add Category</a>
                                </li>
                                <li>
                                    <a href="manageCategory.php">Manage Category</a>
                                </li>
                                <li>
                                    <a href="addQuiz.php">Add Quiz</a>
                                </li>
                                <li>
                                    <a href="manageQuiz.php">Manage Quiz</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> QUESTION<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="addQuestion.php">Add Question</a>
                                </li>
                                <li>
                                    <a href="manageQuestion.php">Manage Question</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> User's Panel<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="addUser.php">Add User</a>
                                </li>
                                <li>
                                    <a href="manageUser.php">Manage User</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>