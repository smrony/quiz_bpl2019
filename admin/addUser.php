<?php include_once('header.php'); ?>
<?php include_once('leftSideNav.php'); ?>
<?php include_once('insertUser.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <?php if($insert){ ?> 
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> A New User Added!!!
                        </div>
                    <?php } ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Enter Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form"  action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">User Role</label>
                                            <select name="role" class="form-control">
                                                <option>Select Role</option> 
                                                <option value="1">Admin</option>
                                                <option value="9">Developer</option>
                                            </select>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name of user">
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Email</label>
                                            <input type="email" class="form-control" name="email"  placeholder="demo@demo.com">
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Password</label>
                                            <input type="password" class="form-control" name="chabi" placeholder="************">
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Status</label>
                                            <select name="status" class="form-control">
                                                <option>Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Deactive</option>
                                            </select>
                                        </div>
                                        <input type="submit" name="addUser" value="Add User" class="btn btn-success btn-md">
                                    </form>
                                </div>
                            </div>
                        </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('footer.php'); ?>