<?php include_once('header.php'); ?>
<?php include_once('leftSideNav.php'); 
$question_id = $_GET['question_id'];
$quiz_id = $_GET['quiz_id'];
?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Result</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>
                        <?php
                            $questions = mysqli_query($con, "SELECT * FROM `questions` WHERE `id`='$question_id'");
                                    while ($question = mysqli_fetch_assoc($questions)) { 
                                        $id = $question['id'];
                                        $category = $question['category'];
                                        $quiz = $question['quiz'];
                                        $question_title = $question['question'];
                                        $right_answer = $question['right_answer'];
                                        echo $question_title;

                                    }
                        ?>
                            </strong>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Quiz Category</th>
                                        <th>Quiz Title</th>
                                        <th>Question</th>                           
                                        <th>Name</th>
                                        <th>Right Answer</th>
                                        <th>Users Answer</th>
                                        <th>Points</th>
                                        <th>Date & Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sl = 1;
                                    $answers = mysqli_query($con, "SELECT * FROM `answers`");
                                    while ($answer = mysqli_fetch_assoc($answers)) {
                                        $id = $answer['id'];
                                        $category_id = $answer['category_id'];
                                        $quiz_id = $answer['quiz_id'];
                                        $question_id = $answer['question_id'];
                                        $member_id = $answer['member_id'];
                                        $right_answer = $answer['right_answer'];
                                        $user_answer = $answer['answer'];
                                        $marks = $answer['marks'];
                                        $submitted_at = $answer['submitted_at']; 
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sl; ?></td>
                                        <td><?php echo $category_id; ?></td>
                                        <td><?php echo $quiz_id; ?></td>
                                        <td><?php echo $question_id; ?></td>
                                        <td><?php echo $member_id; ?></td>
                                        <td><?php echo $right_answer; ?></td>
                                        <td><?php echo $user_answer; ?></td>
                                        <td><?php echo $marks; ?></td>
                                        <td><?php echo $submitted_at; ?></td>
                                        <td class="center">
                                            <a href="#" class="btn btn-block btn-success">Winner</a> 
                                        </td>
                                    </tr>
                                <?php $sl++; } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('footer.php'); ?>