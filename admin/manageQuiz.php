<?php include_once('header.php'); ?>
<?php include_once('leftSideNav.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage Quiz</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Quiz List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Category</th>
                                        <th>Title</th>
                                        <th>Opening Date</th>
                                        <th>Closing Date</th>
                                        <th>Status</th>
                                        <th>Result</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sl = 1;
                                    $quizzes = mysqli_query($con, "SELECT * FROM `quizzes`");
                                    while ($quiz = mysqli_fetch_assoc($quizzes)) {
                                        $quiz_id = $quiz['id'];
                                        $category_id = $quiz['category_id'];
                                        $title = $quiz['title'];
                                        $opening_date = $quiz['opening_date'];
                                        $closing_date = $quiz['closing_date'];
                                        $status = $quiz['status'];
                                        $created_at = $quiz['created_at'];
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sl; ?></td>
                                        <td><?php echo $category_id; ?></td>
                                        <td><?php echo $title; ?></td>
                                        <td><?php echo $opening_date; ?></td>
                                        <td><?php echo $closing_date; ?></td>
                                        <td><?php echo $status; ?></td>
                                        <td class="center">
                                            <a href="quizResult.php?quiz_id=<?php echo $quiz_id; ?>" class="btn btn-block btn-success">Show Result</a>
                                        </td>
                                        <td class="center">
                                            <a href="#" class="">Edit</a>
                                            <a href="#" class="">Delete</a>
                                        </td>
                                    </tr>
                                <?php $sl++; } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('footer.php'); ?>