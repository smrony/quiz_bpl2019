<?php include_once('header.php'); ?>
<?php include_once('leftSideNav.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage Question</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Question List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Category</th>
                                        <th>Title</th>
                                        <th>Question</th>
                                        <th>answer</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Result</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sl = 1;
                                    $questions = mysqli_query($con, "SELECT * FROM `questions`");
                                    while ($question = mysqli_fetch_assoc($questions)) {
                                        $id = $question['id'];
                                        $category_id = $question['category_id'];
                                        $quiz_id = $question['quiz_id'];
                                        $ques = $question['question'];
                                        $right_answer = $question['right_answer'];
                                        $status = $question['status'];
                                        $created_at = $question['created_at'];
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sl; ?></td>
                                        <td><?php echo $category_id; ?></td>
                                        <td><?php echo $quiz_id; ?></td>
                                        <td><?php echo $ques; ?></td>
                                        <td><?php echo $right_answer; ?></td>
                                        <td><?php echo $status; ?></td>
                                        <td><?php echo $created_at; ?></td>
                                        <td class="center">
                                            <a href="questionResult.php?question_id=<?php echo $id; ?>" class="btn btn-block btn-success">Show Result</a>
                                        </td>
                                        <td class="center">
                                            <a href="#" class="">Edit</a>
                                            <a href="#" class="">Delete</a>
                                        </td>
                                    </tr>
                                <?php $sl++; } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('footer.php'); ?>