<?php session_start(); 
include 'adb.php'; 
$user_id = $_SESSION['id'];
unset($_SESSION['id']);
unset($_SESSION['name']); 
session_destroy();
header("Location: login.php");
exit; 