<?php include_once('header.php'); ?>
<?php include_once('leftSideNav.php'); ?>
<?php include_once('insertCategory.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Category</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <?php if($insert){ ?> 
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> A New Category Added!!!
                        </div>
                    <?php } ?>
                    <div class="panel panel-default"> 
                        <div class="panel-heading">
                            Enter Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post"> 
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Category Title</label>
                                            <input type="text" class="form-control" name="categoryTitle" placeholder="BPL T20">
                                        </div> 
                                        <div class="form-group has-success">
                                            <label class="control-label" for="inputSuccess">Status</label>
                                            <select name="status" class="form-control">
                                                <option>Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Deactive</option>
                                            </select>
                                        </div>
                                        <input type="submit" name="addCategory" value="Add Category" class="btn btn-success btn-md">
                                    </form>
                                </div>
                            </div>
                        </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('footer.php'); ?>