<?php include_once('header.php');?>
<?php include_once('reg.php');?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 login-box">
				<div class="login-logo text-center">
			      <img src="img/logo.png" class="">
			    </div>
			    <h3>Member Registration</h3>
			    <p>Become a New Member</p>
				<form method="post" action="reg.php">
					<div class="form-group"> 
				        <input type="text" name="name" class="form-control" placeholder="Full Name" required>
				    </div>
				    <div class="form-group"> 
				        <select name="sex" class="form-control" required>
				        	<option value="">Select Gender</option>
				        	<option value="Male">Male</option>
				        	<option value="Female">Female</option>
				        </select>
				    </div>
				    <div class="form-group">
				        <input type="text" name="cell" class="form-control" placeholder="Cell Number" required>
				    </div>
				    <div class="form-group">
				        <input type="email" name="email" class="form-control" placeholder="Email" required>
				    </div>
				    <div class="form-group"> 
				        <input type="password" name="password" class="form-control" placeholder="Password" required>
				    </div>
				    <div class="form-group"> 
				        <input type="password" name="rPassword" class="form-control" placeholder="Retype Password" required>
				    </div>
				    <div class="form-group">
				    	<button type="submit" name="reg" class="btn btn-primary btn-block btn-flat">Register Now</button>
				    </div>
				    <h3>OR</h3>
				    <div class="form-group">
				    	<a href="login.php" class="btn btn-success btn-block btn-flat">Already Registered Please Login</a>
				    </div>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</section>
<?php include_once('footer.php');?>