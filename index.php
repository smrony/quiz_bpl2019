<?php include_once('header.php'); ?>
    <section class="home-main">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="home-section-main totalQuiz">
              <i class="fas fa-globe"></i>
              <h2><a href="rules.php">Rules</a></h2>
              <hr>
              <h3>___</h3>
            </div>
          </div>
          <div class="col-md-3">
            <div class="home-section-main todaysQuiz">
              <i class="fas fa-download"></i>
              <h2><a href="quizList.php">Today's Quiz</a></h2>
              <hr>
              <h3>___</h3>
            </div>
          </div>
          <div class="col-md-3">
            <div class="home-section-main questions">
              <i class="fa fa-question-circle"></i>
              <h2><a href="winnerList.php">Winners</a></h2>
              <hr>
              <?php
                $winners = mysqli_query($con, "SELECT * FROM `members` WHERE `win`!=0");
                if($winners) {
                  $total_winner = mysqli_num_rows($winners);
                  ?>
                  <h3><?php echo $total_winner; ?></h3>
                <?php } else { ?>
                  <h3>000</h3>
                <?php } ?>
            </div>
          </div>
          <div class="col-md-3">
            <div class="home-section-main members">
              <i class="fas fa-users"></i>
              <h2><a href="members.php">Members</a></h2>
              <hr>
              <?php 
                $members = mysqli_query($con, "SELECT * FROM `members`");
                if($members) {
                  $total_member = mysqli_num_rows($members);
                  ?>
                  <h3><?php echo $total_member; ?></h3>
                <?php } else { ?>
                  <h3>000</h3>
                <?php } ?>
            </div>
          </div>  
        </div>
      </div>
    </section>
<?php include_once('footer.php'); ?>