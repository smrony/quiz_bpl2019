<?php include_once('header.php'); ?>
<div class="mainContainerQuiz">
	<div class="container">
		<div class="well mainSection">
			<h3 class="marginZero">Rules You Have to Follow</h3>
		</div>
		<div class="col-md-9 mainSectionLeft"> 
			<div class="col-md-12 paddingZero">
				<div class="quizSection">
					
					<p>১। কুইজে অংশগ্রহণকারীকে প্রথমে Register করতে হবে। এরপর Log In করলে আপনি কুইজে অংশ নিতে পারবেন। 
					</p>
<p>২। http://www.quiz.dailysportsbd.com এই লিঙ্কে আপনি লগইন করে প্রতিদিনের কুইজে অংশ নিতে পারবেন। </p>
<p>৩। প্রত্যেক অংশগ্রহণকারীর জন্য আলাদা প্রোফাইল থাকবে যেখানে তিনি নিজের ছবি সহ আরও বিস্তারিত তথ্য যোগ  করতে পারবেন। </p>
<p>৪। সিকিউরড প্রোফাইল পাসওয়ার্ড প্রোটেক্টেড হবার কারনে সবার তথ্যসমুহ আন্তর্জাতিক ডিজিটাল প্রোসেসিং সামিট অনুযায়ী সংরক্ষণ করা হবে। কোন ধরনের নিরাপত্তা ঝুকি নেই। </p>
<p>৫। প্রতিদিন ঠিক রাত 10 টার সময় উক্ত দিনের কুইজ দেওয়া হবে। সময় থাকবে পরবর্তী দিন রাত ৮ টা পর্যন্ত। </p>
<p>৬। সঠিক উত্তরদাতাদের নাম লাইভে লটারী করা হবে এবং প্রতিদিনের ৫০ টাকার জন্য বিজয়ী নির্ধারন হবে। </p>
<p>৭। অবশ্যই বাংলাতে প্রশ্ন হলে বাংলাতে উত্তর দিতে হবে । ইংরেজীতে হলে ইংরেজীতে দিতে হবে। সেক্ষেত্রে বড় হাতের অক্ষরে উত্তর দিতে হবে। বানান ভুল অগ্রহনযোগ্য। কোন ধরনের ডাক নাম, শর্ট নাম ব্যাবহার নিষিদ্ধ। যেমন: মুশি/মুশফিক লেখা যাবেনা। মুশফিকুর রহিম লিখতে হবে ।</p>
<p>৮। প্রতিদিনের সঠিক উত্তর দাতাদের পয়েন্ট যোগ হবে। মাসশেষে সব্বোর্চ্চ স্কোর যারা করবেন তারা বিশেষ পুরস্কারের জন্য বিবেচিত হবেন।
</p> 
				</div>
			</div> 
		</div>
		<div class="col-md-3 mainSectionRight">
			<h3 class="marginZero">General Information</h3>
			<?php include_once('rightbarLinks.php'); ?>
			<div class="col-md-12 gads paddingZero">
				<img src="img/gads.jpg" class="img img-responsive">
			</div>
		</div>      
	</div>
</div>
<?php include_once('footer.php'); ?>