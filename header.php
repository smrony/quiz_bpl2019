<?php 
session_start(); 
error_reporting (0);
// error_reporting (E_ALL ^ E_NOTICE);
?>
<?php

include_once('adb.php');

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>CRICMAC | Daily Sports BD | Quiz Portal </title>

    <link rel="icon" href="img/logo.png" type="image/png">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" integrity="sha384-PmY9l28YgO4JwMKbTvgaS7XNZJ30MK9FAZjjzXtlqyZCqBY6X6bXIkM++IkyinN+" crossorigin="anonymous"> 

    <!-- other css -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="css/dataTables.responsive.css" rel="stylesheet">

  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container top-bar">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">CRICMAC</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right"> 
           <?php 
           if(isset($_SESSION['userID'])){
               $userNAME = $_SESSION['userNAME'];
               $userID =$_SESSION['userID'];
           ?>
           <li><a href=""><?php echo $userNAME; ?></a></li>
           <li><a href="logout.php">Logout</a></li>
           <?php }else{ ?> 
            <li><a href="login.php">Login</a></li>
            <li><a href="register.php">Register</a></li>
          <?php } ?>
            
          </ul> 
        </div>
      </div>
    </nav>